// import "aframe";
import React, { Component } from "react";
import "../assets/css/App.css";
import softEyesImage from "../assets/img/face-textures/soft-eyes-mouth.png";
import witchFace from "../assets/img/face-textures/WITCHFACE.png";

import occluder from "../assets/models/head-occluder.glb";
import witchHat from "../assets/models/HATOCT07FINAL.glb";

import $ from "jquery";
// import ScriptTag from 'react-script-tag';

// import DeepAR from "../assets/lib/deepar.js";
import * as DeepAR from "../assets/lib/deepar.js";

class FaceFilter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isCapturing: true,
      isUploading: false,
      imageURL: null,
      isLoading: true,
    };
    this.myCanvasRef = React.createRef();
  }

  // addScript = (path) =>{
  //   const script = document.createElement("script");
  //   script.src = path;
  //   if(path.includes("deep")){
  //     script.onload = () => console.log("done");
  //   }
  //   console.log(path)
  //   document.body.appendChild(script);
  //   return true
  // } 

  async componentDidMount() {
    var canvasHeight = window.innerHeight;
    var canvasWidth = window.innerWidth;
    var deepAR = DeepAR({ 
        canvasWidth: canvasWidth, 
        canvasHeight: canvasHeight,
        licenseKey: '2f3e5466b0ff52b2d94ad027aea14f8093b86fd9865f162ad12f78252963e295b29bac3b4dea30c7',
        canvas: this.myCanvasRef.current,
        // this.myCanvasRef.current,
        numberOfFaces: 1,
        libPath: '../assets/lib',
        segmentationInfoZip: 'segmentation.zip',
        onInitialize: function() {
  
          // start video immediately after the initalization, mirror = true
          deepAR.startVideo(true);
  
          // or we can setup the video element externally and call deepAR.setVideoElement (see startExternalVideo function below)
  
          deepAR.switchEffect(0, 'plot', '../assets/models/glasses', function() {
            // console.log("?awo")
          });
        }
      });
      deepAR.onVideoStarted = () => {
        this.setState({ isLoading : false });
      };
  
      deepAR.downloadFaceTrackingModel('../assets/lib/models-68-extreme.bin');
  }

  render() {
    const { isLoading } = this.state;
    return (
      <div>
        <div>
          {/* <ScriptTag isHydrating={true} type="text/javascript" src="../assets/lib/deepar.js" /> */}
          <canvas className="deepar" ref={this.myCanvasRef} id="deepar-canvas" onContextMenu={event => event.preventDefault()}></canvas>
          <div className={isLoading ? "hidden" : ""} id="loader-wrapper">
            <span className="loader"></span>
          </div>
        </div>
        {/* <Script
          url="//code.jquery.com/jquery-1.11.0.min.js"
        />
        <Script
          url="../assets/lib/deepar.js"
        /> */}
      </div>
    );
  }
}

export default FaceFilter;
